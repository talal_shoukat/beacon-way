package com.coginity.beaconway.beaconway;

/**
 * Created by Usman Shahid on 3/5/2016.
 */
public class Offer{
    private int id;
    private String mainMessage;
    private String subMessage;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMainMessage() {
        return mainMessage;
    }

    public void setMainMessage(String mainMessage) {
        this.mainMessage = mainMessage;
    }

    public String getSubMessage() {
        return subMessage;
    }

    public void setSubMessage(String subMessage) {
        this.subMessage = subMessage;
    }
}
