package com.coginity.beaconway.beaconway;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by Usman Shahid on 3/5/2016.
 */
public class OfferViewHolder extends RecyclerView.ViewHolder{
    ImageView imageView;
    TextView mainText;
    TextView subText;

    public OfferViewHolder(View itemView) {
        super(itemView);
        imageView = (ImageView) itemView.findViewById(R.id.row_offerImage);
        mainText = (TextView) itemView.findViewById(R.id.mainMessage);
        subText = (TextView) itemView.findViewById(R.id.subMessage);
    }

}
