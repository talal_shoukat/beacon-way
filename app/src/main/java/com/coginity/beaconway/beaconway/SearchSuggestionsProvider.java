package com.coginity.beaconway.beaconway;

import android.content.SearchRecentSuggestionsProvider;

/**
 * Created by Usman Shahid on 3/5/2016.
 */
public class SearchSuggestionsProvider extends SearchRecentSuggestionsProvider {
    public final static String AUTHORITY = "com.coginity.beaconway.beaconway.SearchSuggestionsProvider";
    public final static int MODE = DATABASE_MODE_QUERIES;

    public SearchSuggestionsProvider() {
        setupSuggestions(AUTHORITY, MODE);
    }
}
