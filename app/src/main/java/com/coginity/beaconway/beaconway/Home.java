package com.coginity.beaconway.beaconway;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;

import com.kontakt.sdk.android.ble.configuration.ActivityCheckConfiguration;
import com.kontakt.sdk.android.ble.configuration.ForceScanConfiguration;
import com.kontakt.sdk.android.ble.configuration.ScanPeriod;
import com.kontakt.sdk.android.ble.configuration.scan.EddystoneScanContext;
import com.kontakt.sdk.android.ble.configuration.scan.IBeaconScanContext;
import com.kontakt.sdk.android.ble.configuration.scan.ScanContext;
import com.kontakt.sdk.android.ble.connection.OnServiceReadyListener;
import com.kontakt.sdk.android.ble.discovery.BluetoothDeviceEvent;
import com.kontakt.sdk.android.ble.discovery.EventType;
import com.kontakt.sdk.android.ble.filter.eddystone.EddystoneFilters;
import com.kontakt.sdk.android.ble.filter.eddystone.URLFilter;
import com.kontakt.sdk.android.ble.manager.ProximityManager;
import com.kontakt.sdk.android.ble.manager.ProximityManagerContract;
import com.kontakt.sdk.android.ble.spec.EddystoneFrameType;
import com.kontakt.sdk.android.common.KontaktSDK;
import com.kontakt.sdk.android.common.model.ContentAction;
import com.kontakt.sdk.android.common.model.IAction;
import com.kontakt.sdk.android.common.model.IDevice;
import com.kontakt.sdk.android.common.profile.DeviceProfile;
import com.kontakt.sdk.android.common.profile.RemoteBluetoothDevice;
import com.kontakt.sdk.android.http.HttpResult;
import com.kontakt.sdk.android.http.KontaktApiClient;
import com.kontakt.sdk.android.http.RequestDescription;
import com.kontakt.sdk.android.http.exception.ClientException;
import com.kontakt.sdk.android.http.interfaces.ActionsApiAccessor;
import com.kontakt.sdk.android.http.interfaces.ResultApiCallback;
import com.kontakt.sdk.android.manager.KontaktProximityManager;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class Home extends AppCompatActivity implements ProximityManager.ProximityListener {

    private static final String TAG = Home.class.getSimpleName();

    private ProximityManagerContract proximityManager;

    private List<IDevice> allDevices;

    private KontaktApiClient kontaktApiClient;
    OnSwitchClickListner switchlistner;
    public Context context;
    ImageView imgview;
    EditText editText;
    Drawable image;
    List<URLFilter> filterList = Arrays.asList(
            EddystoneFilters.newURLFilter("http://kntk.io/eddystone")
            //accept Eddystone devices only with specified url addresses
    );



    // private ScanContext scanContext;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        switchlistner=new OnSwitchClickListner(this);
        setContentView(R.layout.activity_home);
        KontaktSDK.initialize("vOSNsoprmUCkJxlPTDgkWuNonkIEqmVu");
        proximityManager = new KontaktProximityManager(this);
        allDevices = new ArrayList<>();
        kontaktApiClient = new KontaktApiClient();
        fetchDevices(0);

    }

    @Override
    protected void onStart() {
        super.onStart();
        proximityManager.initializeScan(getScanContext(), new OnServiceReadyListener() {
            @Override
            public void onServiceReady() {
                proximityManager.attachListener(Home.this);
            }

            @Override
            public void onConnectionFailure() {

            }
        });
    }


    private void fetchDevices(int offset) {
        RequestDescription requestDescription = RequestDescription.start()
                .setStartIndex(offset)
                .build();

        kontaktApiClient.listDevices(requestDescription, new ResultApiCallback<List<IDevice>>() {
            @Override
            public void onSuccess(HttpResult<List<IDevice>> result) {
                if (result.isPresent()) {
                    allDevices.addAll(result.get());
                    int offset = result.getSearchMeta().getOffset();
                    int startIndex = result.getSearchMeta().getStartIndex();
                    if (result.getSearchMeta().hasNextResultsURI()) {
                        fetchDevices(startIndex + offset);
                    } else {
                        Log.d(TAG, "fetched devices count=" + allDevices.size());
                    }
                }
            }

            @Override
            public void onFailure(ClientException e) {
                e.printStackTrace();
            }
        });



    }
    private void GetDevices(List<? extends RemoteBluetoothDevice> deviceList)
    {
        ActionsApiAccessor kontaktApiClient = new KontaktApiClient();

        // if(deviceList!=null) deviceList.get(0).getUniqueId()
        kontaktApiClient.getActionsForDevice(deviceList.get(0).getUniqueId(), new ResultApiCallback<List<IAction>>() {
            @Override
            public void onSuccess(HttpResult<List<IAction>> result) {
                image = null;
                try {

                    URL url = new URL(((ContentAction) result.get().get(0)).getContentUrl());
                    InputStream is = (InputStream) url.getContent();
                    image = Drawable.createFromStream(is, "src");
                } catch (MalformedURLException e) {
                    // handle URL exception
                    image = null;
                } catch (IOException e) {
                    // handle InputStream exception
                    image = null;
                }
                imgview = (ImageView) findViewById(R.id.imageView);

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        imgview.setImageDrawable(image);

                    }
                });
            }

            @Override
            public void onFailure(ClientException e) {

            }
        });
    }



    @Override
    protected void onStop() {
        super.onStop();
        proximityManager.detachListener(this);
        proximityManager.disconnect();
    }

    private ScanContext getScanContext() {
        if (scanContext == null) {
            scanContext = new ScanContext.Builder()
                    .setScanPeriod(ScanPeriod.RANGING) // or for monitoring for 15 seconds scan and 10 seconds waiting:
                            //.setScanPeriod(new ScanPeriod(TimeUnit.SECONDS.toMillis(15), TimeUnit.SECONDS.toMillis(10)))
                    .setScanMode(ProximityManager.SCAN_MODE_BALANCED)
                    .setActivityCheckConfiguration(ActivityCheckConfiguration.MINIMAL)
                    .setForceScanConfiguration(ForceScanConfiguration.MINIMAL)
                    .setIBeaconScanContext(new IBeaconScanContext.Builder().build())
                    .setEddystoneScanContext(new EddystoneScanContext.Builder().build())
                    .setForceScanConfiguration(ForceScanConfiguration.MINIMAL)
                    .build();
        }
        return scanContext;
    }

    Collection<EddystoneFrameType> eddystoneFrameTypes = Arrays.asList(EddystoneFrameType.UID, EddystoneFrameType.URL, EddystoneFrameType.TLM);


    EddystoneScanContext eddystoneScanContext = new EddystoneScanContext.Builder()
            .setEventTypes(Arrays.asList(
                    EventType.SPACE_ENTERED,
                    EventType.DEVICE_DISCOVERED,
                    EventType.SPACE_ABANDONED))
            .setTriggerFrameTypes(eddystoneFrameTypes)
            .setURLFilters(filterList)
            .build();

    ScanContext scanContext = new ScanContext.Builder()
            .setEddystoneScanContext(eddystoneScanContext)
            .build();




    @Override
    public void onEvent(BluetoothDeviceEvent bluetoothDeviceEvent) {
        List<? extends RemoteBluetoothDevice> deviceList = bluetoothDeviceEvent.getDeviceList();
        if(!deviceList.isEmpty())
        {
            /*
            to display venue
            int count=0;
            for(int i = 0; i<allDevices.size();i++ )
            {
                if(deviceList.get(0).getUniqueId()==allDevices.get(count).getUniqueId())
                {
                    Venue venue=(Venue)allDevices.get(count).getVenue();
                    editText= (EditText) findViewById(R.id.editText);
                    editText.setText(venue.getDescription());
                }
                count++;
            }*/
            GetDevices(deviceList);
        }
        long timestamp = bluetoothDeviceEvent.getTimestamp();
        DeviceProfile deviceProfile = bluetoothDeviceEvent.getDeviceProfile();
        switch (bluetoothDeviceEvent.getEventType()) {
            case SPACE_ENTERED:
                Log.d(TAG, "namespace or region entered");
                break;
            case DEVICE_DISCOVERED:
                Log.d(TAG, "found new beacon");
                break;
            case DEVICES_UPDATE:
                Log.d(TAG, "updated beacons");
                break;
            case DEVICE_LOST:
                Log.d(TAG, "lost device");
                break;
            case SPACE_ABANDONED:
                Log.d(TAG, "namespace or region abandoned");
                break;
        }
    }

    @Override
    public void onScanStart() {
        Log.d(TAG, "scan started");
    }

    @Override
    public void onScanStop() {
        Log.d(TAG, "scan stopped");
    }
}

