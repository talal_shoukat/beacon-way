package com.coginity.beaconway.beaconway;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Usman Shahid on 3/5/2016.
 */
public class NotificationAdapter extends RecyclerView.Adapter<OfferViewHolder> {

    private Offer[] offers;
    private View.OnClickListener onClickListener;

    public NotificationAdapter(Offer[] inOffers, View.OnClickListener listener){
        offers = inOffers;
        onClickListener = listener;
    }


    @Override
    public OfferViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.offer_row, parent, false);
        itemView.setOnClickListener(onClickListener);
        return new OfferViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OfferViewHolder holder, int position) {
        Offer currentOffer = offers[position];
        holder.imageView.setImageResource(currentOffer.getId());
        holder.imageView.setTag(currentOffer.getId());
        holder.mainText.setText(currentOffer.getMainMessage());
        holder.subText.setText(currentOffer.getSubMessage());
    }

    @Override
    public int getItemCount() {
        return offers.length;
    }
}
