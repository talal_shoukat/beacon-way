package com.coginity.beaconway.beaconway;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageView;

/**
 * Created by Usman Shahid on 3/5/2016.
 */
public class OnAddClickListener implements View.OnClickListener {

    public Context context;

    public OnAddClickListener(Context appContext){
        context = appContext;
    }

    @Override
    public void onClick(View v) {
        ImageView imgView = (ImageView) v.findViewById(R.id.row_offerImage);

        Intent intent = new Intent(context, OfferView.class);
        int imageId =  (Integer)imgView.getTag();
        intent.putExtra("ImageId",imageId);
        context.startActivity(intent);
    }
}
