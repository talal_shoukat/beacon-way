package com.coginity.beaconway.beaconway;

import android.app.SearchManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.provider.SearchRecentSuggestions;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

public class Search extends AppCompatActivity {

    public TouchImageView image;
    public Bitmap WalMap;
    public Paint paint;
    public Bitmap bMap;
    public Canvas canvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Intent intent = getIntent();

        image = (TouchImageView) findViewById(R.id.map);
        WalMap = BitmapFactory.decodeResource(getResources(), R.drawable.mallmap);
        paint = new Paint(Paint.FILTER_BITMAP_FLAG);
        paint.setStrokeWidth(10);
        //paint.setColor(Color.parseColor("#CD5C5C"));
        paint.setColor(Color.BLUE);
        bMap = Bitmap.createBitmap(WalMap.getWidth(), WalMap.getHeight(), Bitmap.Config.RGB_565);
        int width=WalMap.getWidth();
        int height=WalMap.getHeight();
        Canvas canvas = new Canvas(bMap);

        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    SearchSuggestionsProvider.AUTHORITY, SearchSuggestionsProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            doMySearch(query, width, height, canvas);
        }
    }

    public void doMySearch(String query, int w, int h, Canvas canvas){

        query=query.toUpperCase();
        if(query.contains("WALMART"))
        {
            canvas.drawBitmap(WalMap, 0, 0, null);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.location);
            Bitmap dest = BitmapFactory.decodeResource(getResources(), R.drawable.destination);

            canvas.drawBitmap(icon,w / 2 + 110-icon.getWidth()+15, h / 4-icon.getHeight()+35,paint);
            canvas.drawBitmap(dest,w/2-192, h*9/12+20,paint);

            canvas.drawLine(w / 2 + 110, h / 4 + 28, w / 2 + 110, h * 9 / 12 + 20, paint);
            canvas.drawLine(w/2+110,h*9/12+20,w/2-180,h*9/12+20,paint);
            image.setImageDrawable(new BitmapDrawable(getResources(), bMap));
        }
        else if(query.contains("SPORT"))
        {
            canvas.drawBitmap(WalMap, 0, 0, null);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.location);
            canvas.drawBitmap(icon,w / 2 + 110-icon.getWidth()+15, h / 4-icon.getHeight()+35,paint);

            Bitmap dest = BitmapFactory.decodeResource(getResources(), R.drawable.destination);
            canvas.drawBitmap(dest,w/2+195, h*9/12+35,paint);

            canvas.drawLine(w / 2 + 110, h / 4 + 28, w / 2 + 110, h * 9 / 12 + 35, paint);
            canvas.drawLine(w/2+110,h*9/12+35,w/2+220,h*9/12+35,paint);
            image.setImageDrawable(new BitmapDrawable(getResources(), bMap));
        }
        else if(query.contains("H&M") || query.contains("H and M"))
        {
            canvas.drawBitmap(WalMap, 0, 0, null);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.location);
            canvas.drawBitmap(icon,w / 2 + 110-icon.getWidth()+15, h / 4-icon.getHeight()+35,paint);

            Bitmap dest = BitmapFactory.decodeResource(getResources(), R.drawable.destination);
            canvas.drawBitmap(dest,w / 2 + 70, h * 9 / 12 + 5,paint);

            canvas.drawLine(w / 2 + 110, h / 4 + 28, w / 2 + 110, h * 9 / 12 + 40, paint);
            image.setImageDrawable(new BitmapDrawable(getResources(), bMap));
        }
        else if(query.contains("SEARS"))
        {
            canvas.drawBitmap(WalMap, 0, 0, null);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.location);
            canvas.drawBitmap(icon,w / 2 + 110-icon.getWidth()+15, h / 4-icon.getHeight()+35,paint);

            Bitmap dest = BitmapFactory.decodeResource(getResources(), R.drawable.destination);
            canvas.drawBitmap(dest,w / 2 + 370, h/4 + 100,paint);


            canvas.drawLine(w / 2 + 110, h / 4+ 28, w / 2 + 110, h / 4 + 100, paint);
            canvas.drawLine(w / 2 + 110, h/4 +100, w / 2 + 400, h/4 +100, paint);
            image.setImageDrawable(new BitmapDrawable(getResources(), bMap));
        }
        else if(query.contains("TOYS"))
        {
            canvas.drawBitmap(WalMap, 0, 0, null);
            Bitmap icon = BitmapFactory.decodeResource(getResources(), R.drawable.location);
            canvas.drawBitmap(icon,w / 2 + 110-icon.getWidth()+15, h / 4-icon.getHeight()+35,paint);

            Bitmap dest = BitmapFactory.decodeResource(getResources(), R.drawable.destination);
            canvas.drawBitmap(dest,w / 2 - 140, h/4 + 70,paint);

            canvas.drawLine(w / 2 + 110, h / 4+28, w / 2 + 110, h / 4 + 70, paint);
            canvas.drawLine(w / 2 + 110, h / 4 + 70, w / 2 - 110, h / 4 + 70, paint);
            image.setImageDrawable(new BitmapDrawable(getResources(), bMap));
        }
        else
        {
            Toast.makeText(getApplicationContext(), query, Toast.LENGTH_LONG).show();
        }

    }
}
